"use strict";
/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
exports.id = "locales_eo_common_json";
exports.ids = ["locales_eo_common_json"];
exports.modules = {

/***/ "./locales/eo/common.json":
/*!********************************!*\
  !*** ./locales/eo/common.json ***!
  \********************************/
/***/ ((module) => {

module.exports = JSON.parse('{"metaTitle":"Next.js Lokigo kun Next-Translate","title":"Bonvenon al mia aplikaĵo i18n NextJS!","description":"Mi uzas la bibliotekon Next-Translate por traduki ĉi tiun paĝon."}');

/***/ })

};
;
import useTranslation from 'next-translate/useTranslation';
import Head from 'next/head';
import Link from 'next/link';
import styles from '../styles/Home.module.css';
export default function Home() {
const { t } = useTranslation('common');
  return (
    <div className={styles.container}>
      <Head>
        <title>{t('metaTitle')}</title>
      </Head>
      <main className={styles.main}>
        <h2 className={styles.title}>{t('title')}</h2>
        <p className={styles.description}>{t('description')}</p>
        <Link href="/" locale="fr">
          <p><button>Français</button></p>
        </Link>
        <Link href="/" locale="ja">
          <p><button>日本語</button></p>
        </Link>
        <Link href="/" locale="en">
          <p><button>English</button></p>
        </Link>
        <Link href="/" locale="eo">
          <p><button>Esperanto</button></p>
        </Link>
<p>May/16/2022 PM 19:37</p>
      </main>
    </div>
  );
}
